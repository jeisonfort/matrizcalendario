/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package matrizcalendario;

/**
 *
 * @author jeiso
 */
public class Calendario {
    
    private Mes[] calendario;

    public Mes[] getCalendario() {
        return calendario;
    }

    public void setCalendario(Mes[] calendario) {
        this.calendario = calendario;
        
    }

    public int getAnio() {
        return anio;
    }

    public void setAnio(int anio) {
        this.anio = anio;
    }
    private int anio;
    
    
    public Calendario(int pAnio) throws Exception{
        this.calendario=new Mes[12];
        this.anio=pAnio;
        llenarCalendario();
    }
    /**
     * LLena el arreglo con los meses del año. 
     * @throws Exception Si no es un mes valido. 
     */
    private void llenarCalendario() throws Exception{
        for(int i=0;i<this.calendario.length;i++){
            Mes mes = new Mes(this.anio,i);
            this.calendario[i]=mes;
        }
    
    }
    /**
     * Retorna si el año es bisiesto
     * @return true si es bisiesto o false si no lo es.
     */
    public boolean esBisiesto(){
        boolean bisiesto=false;
        
        String anio = String.valueOf(this.anio);
        int digitos = this.anio%100;
        if(this.anio%1000!=0) bisiesto=digitos%4==0;
        else bisiesto=this.anio%400==0;
        
        return bisiesto;
    }
    /**
     * Busca el mes por la posicion 
     * @param pMes (0 es Enero, 1 es Febrero , 2 es Marzo ... )
     * @return Retorna objeto de tipo Mes. 
     * @throws Exception Si se ingresa un valor que no corresponde a ningun mes. 
     */
    public Mes buscarMes(int pMes) throws Exception{
        try{
            return this.calendario[pMes];
        }
        catch(Exception s){
            throw new Exception("El mes ingresado no tiene el formato correcto o no es un mes valido");
        }
        
    }
    
    @Override
    public String toString(){
        String msg="";
        
        for (Mes mes : calendario) {
            msg+=mes.toString();
            msg+="\n";
            msg+="******************";
            msg+="\n";
        }
        return msg;
    }
}
