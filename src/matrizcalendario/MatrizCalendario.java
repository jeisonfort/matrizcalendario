/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package matrizcalendario;

import java.util.Scanner;

/**
 *
 * @author jeiso
 */
public class MatrizCalendario {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        boolean salir = false;
        while (!salir) {
            try {
                Scanner sc = new Scanner(System.in);
                System.out.println("Digite el año: ");
                int anio = Integer.parseInt(sc.nextLine());

                Calendario calendario = new Calendario(anio);
                System.out.println("1. Buscar dia");
                System.out.println("2. Buscar dias ");
                System.out.println("3. Consultar si el año es bisiesto");
                System.out.println("4. Meses con 31 dias");
                System.out.println("5. Mostrar Calendario");
                String opcion = sc.nextLine();
                switch (opcion) {
                    case "1":
                        System.out.println("Digite el mes de la busqueda");
                        System.out.println("1.Enero : 2.Febrero : 3.Marzo ...");
                        int pmes = Integer.parseInt(sc.nextLine()) - 1;
                        Mes mes = calendario.buscarMes(pmes);
                        System.out.println("Ingrese el día buscado");
                        System.out.println("Ej: 1 , 2, 3 , 4...");
                        System.out.println(mes.buscarNombreDia(Integer.parseInt(sc.nextLine())));
                        break;

                    case "2":
                        System.out.println("Digite numero del mes");
                        System.out.println("1.Enero : 2.Febrero : 3.Marzo ...");
                        int pmes2 = Integer.parseInt(sc.nextLine()) - 1;
                        Mes mes2 = calendario.buscarMes(pmes2);
                        System.out.println("¿Qué día de la semana está buscado?");
                        System.out.println("Ej: Lunes, Martes, Miercoles...");
                        String dia = sc.nextLine();
                        int lista[] = mes2.getDias(dia.toLowerCase());
                        for (int d : lista) {
                            System.out.print(d + " ");
                        }
                        System.out.println();
                        break;

                    case "3":
                        if (calendario.esBisiesto()) {
                            System.out.println("Es bisiesto");
                        } else {
                            System.out.println("No es bisiesto");
                        }
                        break;

                    case "4":
                        System.out.println("Enero, marzo, mayo, julio, agosto, octubre y diciembre.");
                        break;//1, 3, 5, 7,8,10,12

                    case "5":
                        System.out.println(calendario.toString());
                        break;
                        
                    default:
                        System.out.println("No es una opcion valida :(");
                }

                System.out.println("¿Desea salir del programa? (1. Si : 2. No)");
                String psalir = sc.nextLine();
                switch (psalir) {
                    case "1":
                        salir = true;
                        break;

                    case "2":
                        salir = false;
                        break;
                }

            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }

        }

    }

}
