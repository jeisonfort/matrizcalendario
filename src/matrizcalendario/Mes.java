/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package matrizcalendario;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 *
 * @author jeiso
 */
public class Mes {

    private int[][] dias;

    String nombre;
    private int anio;
    private int mes;

    public Mes(int pAnio, int pMes) throws Exception {
        this.dias = new int[6][7];
        this.anio = pAnio;
        this.mes = pMes;
        nombreMes(); //Asigna el nombre del mes.
        llenarMatriz(); //LLena la matriz. 
        
    }
    /**
     * Llena la matriz de los dias en base a al año y mes correspondiente. 
     * @throws Exception 
     */
    public void llenarMatriz() throws Exception {

        GregorianCalendar diaMes = new GregorianCalendar(this.anio, this.mes, 0);
        int primero = diaMes.get(Calendar.DAY_OF_WEEK); //Día de la semana al que corresponde el  primer día del mes.      
        int limite=diasDelMes();  //Cantidad del dias que tiene el mes.      
        llenar(primero,limite);

    }

    /**
     * Llena la matriz hasta el limite de dias que le correspondan al mes
     * @param primero Dia de la semana con el que comienza el mes. (0 es domingo, 1 es lunes, 2 martes ...)
     * @param limite Limite de dias que tenga el mes. (28,29,30 o 31 dias)
     */
    private void llenar(int primero,int limite) {

        int diaMes = 1;
        for (int i = 0; i < this.dias.length && diaMes<=limite; i++) {
            if (i == 0) {//LLena la primera fila 
                for (int j = primero; j < this.dias[i].length; j++) {
                    this.dias[i][j] = diaMes;
                    diaMes++;
                }
            } else {//LLena todas las demas filas.
                for (int j = 0; j < this.dias[i].length && diaMes<=limite; j++) {
                    this.dias[i][j] = diaMes;
                    diaMes++;
                }
            }

        }

    }
    /**
     * Devuelve el numero de dias que tiene el mes.
     * @return numeroDias : numeros de dias del mes.
     * @throws Exception No es un mes valido.
     */
    public int diasDelMes() throws Exception{
        int numeroDias=0;
       switch (this.mes) {
            case 0, 2, 4, 6, 7, 9, 11 ->{
                numeroDias = 31;
            }
            case 3, 5, 8, 10 ->{
                numeroDias = 30;
            }
            case 1 ->{
                if (((this.anio % 4 == 0) &&
                        !(this.anio % 100 == 0))
                        || (this.anio % 400 == 0))
                    numeroDias = 29;
                else
                    numeroDias = 28;
            }
            default ->{
                throw new Exception("No es un mes valido");

            }
        }
       return numeroDias;
    }
    /**
     * Busca el dia de la semana al que pertenezca el dia del mes. 
     * @param dia Día del mes.
     * @return (Lunes, martes, miercoles...)
     * @throws Exception 
     */
    public String buscarNombreDia(int dia) throws Exception {

        String msg = "";

        for (int i = 0; i < this.dias.length; i++) {

            for (int j = 0; j < this.dias[i].length; j++) {
                if (dia == this.dias[i][j]) {
                    msg=darNombreDia(j);
                }
            }
        }

        return msg;

    }
    /**
     * Devuelve un Array con todos los dias del mes que sean el dia de la semana que requiera.  
     * @param pDia (Lunes, Martes, Miercoles...)
     * @return Array de dias.
     * @throws Exception 
     */
    public int [] getDias(String pDia) throws Exception{
        int dia = darDiaPorNombre(pDia);
        int lista[]=new int[this.dias.length];
        for(int i =0;i<this.dias.length;i++){
            int nuevo =this.dias[i][dia];
            lista[i]=nuevo;
      
        }
        return lista;
    }
    /**
     *  Dar nombre del día de acuerdo a la columna en la que se encuentre.
     * @param pos Columna en la que está el día. 
     * @return (Domingo, lunes, martes...)
     * @throws Exception 
     */
    private String darNombreDia(int pos) throws Exception{
        
        String msg ="";
        switch (pos) {
                        case 0:
                            msg = "Domingo";
                            break;
                        case 1:
                            msg = "Lunes";
                            break;
                        case 2:
                            msg = "Martes";
                            break;
                        case 3:
                            msg = "Miercoles";
                            break;
                        case 4:
                            msg = "Jueves";
                            break;
                        case 5:
                            msg = "Viernes";
                            break;
                        case 6:
                            msg = "Sabado";
                            break;
                            
                        default:
                            throw new Exception("No es un día valido");

                    }
        return msg;
    }
    /**
     * De acuerdo al valor string del día retorna el entero que le corresponde
     * @param pos
     * @return 0 es Domingo, 1 es Lunes, 2 es Martes ... 
     * @throws Exception Si no corresponde a ningún día. 
     */
    private int darDiaPorNombre(String pos) throws Exception{
    
        int msg =0;
        switch (pos) {
                        case "domingo":
                            msg = 0;
                            break;
                        case "lunes":
                            msg = 1;
                            break;
                        case "martes":
                            msg = 2;
                            break;
                        case "miercoles":
                            msg = 3;
                            break;
                        case "jueves":
                            msg = 4;
                            break;
                        case "viernes":
                            msg = 5;
                            break;
                        case "sabado":
                            msg = 6;
                            break;
                        default:
                            throw new Exception("Dia de la semana no valido");

                    }
        return msg;
    }
    /**
     * Asigna el nombre al mes en base a su valor en int. 
     * Por ejemplo: 0 es enero, 1 es febrero, 2 es marzo ...
     */
    public void nombreMes(){
        switch (mes) {
            case 0:
                nombre = "Enero";
                break;
            case 1:
                nombre = "Febrero";

                break;
            case 2:
                nombre = "Marzo";

                break;
            case 3:
                nombre="Abril";

                break;
            case 4:
                nombre="Mayo";

                break;
            case 5:
                nombre= "Junio";

                break;
            case 6:
                nombre = "Julio";
                break;
            case 7:
                nombre = "Agosto";
                break;

            case 8:
                nombre = "Septiembre";
                break;
            case 9:
                nombre = "Octubre";
                break;

            case 10:
                nombre = "Noviembre";
                break;

            case 11:
                nombre ="Diciembre";
                break;
            
        }
        
    }
    
    @Override
    public String toString(){
        String msg =nombre;
        msg+="\n";
        msg+="D"+"\t"+"L"+"\t"+"M"+"\t"+"M"+"\t"+"J"+"\t"+"V"+"\t"+"S";
        msg+="\n";
        for (int[] dia : dias) {
            for (int i : dia) {
                if(i==0)msg+="\t";
                else msg+=i+ "\t";
            }
            msg+="\n";
        }
        return msg;
    }

}
